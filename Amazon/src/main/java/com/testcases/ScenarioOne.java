package com.testcases;

import java.io.IOException;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import com.Amazon.base.Baseclass;
import com.Amazon.pom.ScenarioOnePom;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;

public class ScenarioOne extends Baseclass{

	@Test
	public static void myTest() throws InterruptedException, IOException {
		
		ScenarioOnePom pom=new ScenarioOnePom(driver);

		logger = extent.createTest("Click on english");
		pom.englishClick();
		
		logger = extent.createTest("Click on Sign In");
		pom.signInClick();
		
		logger = extent.createTest("Search item in Searchbar");
		pom.searchSendKeys();
		
		logger = extent.createTest("Click on second element");
		pom.secondElementClick();

		logger = extent.createTest("Location");
		pom.locationClick();
		
		logger = extent.createTest("Pin code click");
		Thread.sleep(2000);
		AndroidElement pinCode = driver.findElement(By.id("in.amazon.mShop.android.shopping:id/loc_ux_pin_code_text_pt1"));
		pinCode.clear();
		pinCode.sendKeys(getData("pinCode"));
		
		logger = extent.createTest("Click on Apply button");
		pom.applyClick();
		
//		Thread.sleep(2000);
//		AndroidElement text1 = driver.findElement(By.id("mobileQuantitySelection"));
//		System.out.println("text1="+text1.getText());
//		
		Thread.sleep(2000);
		AndroidElement text2 = driver.findElement(By.xpath("//android.widget.Spinner[@text='1']"));
		System.out.println("text2="+text2.getText());
		

			
	}
}
