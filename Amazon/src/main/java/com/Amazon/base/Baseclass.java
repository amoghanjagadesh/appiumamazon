package com.Amazon.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;


public class Baseclass {
	protected static AppiumDriver<AndroidElement> driver;
	protected static WebDriverWait wait;
	protected static ExtentReports extent;
	protected static ExtentTest logger;

	@BeforeTest
	public static void myDriver() throws InterruptedException, IOException {

		DesiredCapabilities capabilities = new DesiredCapabilities();
//		capabilities.setCapability("deviceName", "emulator-5554");
		capabilities.setCapability("noReset", "false");
		capabilities.setCapability("deviceName", "51bec6ac");
		capabilities.setCapability("platformVersion", "9.0");
		capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("App", Baseclass.getData("path"));
		capabilities.setCapability("appPackage", "in.amazon.mShop.android.shopping");
		capabilities.setCapability("appActivity", "com.amazon.mShop.splashscreen.StartupActivity");
		capabilities.setCapability("newCommandTimeout", "60");
		capabilities.setCapability("unicodeKeyboard", "true");
		capabilities.setCapability("resetKeyboard", "true");

		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AndroidDriver(url, capabilities);

		Thread.sleep(2000);

	}

	// Basic info of the report
	@BeforeClass
	public void startReport() {
		ExtentHtmlReporter htmlReporter = new ExtentHtmlReporter("./Reports/automation.html");
		extent = new ExtentReports();
		extent.attachReporter(htmlReporter);
		extent.setSystemInfo("OS", "Microsoft Windows 10");
		extent.setSystemInfo("Host Name", "Amogha N J");
		extent.setSystemInfo("Environment", "QA");
		extent.setSystemInfo("Emai ID", "amoghanjagadesh@gmail.com");
		htmlReporter.config().setDocumentTitle("Automation Testing Report");
		htmlReporter.config().setReportName("Amazon");
	}

	// Taking screen shot and loggers of the report
	@AfterMethod
	public void getResult(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			String temp = getScreenshot((AndroidDriver) driver);
			logger.fail(result.getThrowable().getMessage(),
					MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
			logger.log(Status.FAIL, "Test Case Failed is " + result.getName());
		} else if (result.getStatus() == ITestResult.SKIP) {
			logger.log(Status.SKIP, "Test Case Skipped is " + result.getName());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
			logger.log(Status.PASS, "Test Case Passed is " + result.getName());
		}
	}

	// Flush extent report to print all the data to the automation.html report
	@AfterTest
	public void endReport() {
		extent.flush();
	}

	//Method to take screenshot report
	public static String getScreenshot(AndroidDriver driver) {
		
		TakesScreenshot ts = (TakesScreenshot) driver;

		File src = ts.getScreenshotAs(OutputType.FILE);

		String path = System.getProperty("user.dir") + "/Screenshot/" + System.currentTimeMillis() + ".png";
		File destination = new File(path);

		try {
			FileUtils.copyFile(src, destination);
		} catch (IOException e) {
			System.out.println("Capture Failed " + e.getMessage());
		}
		return path;
	}

	// Read properties file data
	public static String getData(String text) throws IOException {
		FileInputStream fis = new FileInputStream("./data.properties");
		Properties data = new Properties();
		data.load(fis);
		return data.getProperty(text);
	}

	//Wait for the element method
	public void waitForElement(AndroidElement element) throws InterruptedException {
		Thread.sleep(2000);
		wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));

	}

	//Click for element
	public void clickForElement(AndroidElement element) throws InterruptedException {
		waitForElement(element);
		if (element.isDisplayed()) {
			element.click();
		}
	}

	//Send keys for element
	public void sendKeysForElement(AndroidElement element, String text) throws InterruptedException {
		waitForElement(element);
		element.sendKeys(text);
		Thread.sleep(3000);
		((AndroidDriver<AndroidElement>) driver).pressKey(new KeyEvent(AndroidKey.DPAD_CENTER));
		((AndroidDriver<AndroidElement>) driver).pressKey(new KeyEvent(AndroidKey.DPAD_DOWN));
		((AndroidDriver<AndroidElement>) driver).pressKey(new KeyEvent(AndroidKey.ENTER));
	}
}