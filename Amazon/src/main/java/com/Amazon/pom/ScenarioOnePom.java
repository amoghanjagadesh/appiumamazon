package com.Amazon.pom;

import java.io.IOException;

import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.Amazon.base.Baseclass;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;


public class ScenarioOnePom extends Baseclass{

	public ScenarioOnePom(AppiumDriver<AndroidElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@FindBy(xpath = "(//android.widget.ImageView[@index='0'])[2]")
	private AndroidElement english;

//	@FindBy(id = "in.amazon.mShop.android.shopping:id/sign_in_button")
//	private AndroidElement signIn;
	
	@FindBy(id = "in.amazon.mShop.android.shopping:id/skip_sign_in_button")
	private AndroidElement skipSignIn;
	
	@FindBy(xpath = "//android.widget.EditText[@index='0']")
	private AndroidElement search;
	
	@FindBy(xpath = "//android.view.View[@index='17']")
	private AndroidElement secondElement;
	
	@FindBy(xpath = "//android.widget.Button[@index='2']")
	private AndroidElement location;
	
	@FindBy(xpath = "in.amazon.mShop.android.shopping:id/loc_ux_pin_code_text_pt1")
	private AndroidElement pinCode;
	
	//android.widget.Button[@index='2']
	@FindBy(id = "in.amazon.mShop.android.shopping:id/loc_ux_update_pin_code")
	private AndroidElement apply;
	
	
	public void englishClick() throws InterruptedException {
		clickForElement(english);
	}

//	public void signInClick() {
//		clickForElement(signIn);
//	}
	
	public void signInClick() throws InterruptedException {
		clickForElement(skipSignIn);
	}
	
	public void searchSendKeys() throws IOException, InterruptedException {
		sendKeysForElement(search,getData("mySearch"));
	}
	
	public void secondElementClick() throws InterruptedException {
		clickForElement(secondElement);
	}
	
	public void locationClick() throws InterruptedException {
		clickForElement(location);
	}
	
//	public void pincodeSendkeys() throws IOException, InterruptedException {
//		simpleSendKeys(pinCode,getData("pinCode"));
//	}
	
	public void applyClick() throws InterruptedException {
		Thread.sleep(2000);
		clickForElement(apply);
	}
}
